#include <iostream>
#include <cstddef>
#include <cassert>
#include <fstream>

#include <gtest/gtest.h>

#include "Emu6502/cpu.h"
#include "Emu6502/instructions.h"


class TestFixtureBasicInit : public ::testing::Test
{
protected:
    TestFixtureBasicInit()
    {
        m_mem.setWord(0xFFFC, 0x200);
        m_cpu.reset(m_mem);
    }

    void pushProgramByte(byte_t byte)
    {
        m_mem[m_pcWrite] = byte;
        ++m_pcWrite;
    }

    void pushProgramWord(word_t word)
    {
        m_mem.setWord(m_pcWrite, word);
        m_pcWrite += 2;
    }
    static constexpr word_t PC_INIT = 0x0200;
    word_t m_pcWrite = PC_INIT;

    Mem m_mem;
    Cpu m_cpu;
};

TEST(Tests, Mem1)
{
    Mem mem;

    mem[0] = 0x80;
    mem[1] = 0x40;
    EXPECT_EQ(mem.getWord(0), 0x4080);

    mem.setWord(0, 0x4080);
    EXPECT_EQ(mem.getWord(0), 0x4080);
}

TEST_F(TestFixtureBasicInit, LDA_Immediate)
{
    ASSERT_EQ(m_cpu.pc, 0x0200);

    auto itInstruction = Instructions::findWithTypeAndMode(InstructionType::LDA, AddressingMode::Immediate);

    ASSERT_NE(itInstruction, Instructions::invalid());

    pushProgramByte(itInstruction->opCode);
    pushProgramByte(0x42);

    m_cpu.execute(itInstruction->cycles, m_mem);

    m_mem.dumpToFile();

    ASSERT_EQ(m_cpu.reg.a, 0x42);
}

TEST_F(TestFixtureBasicInit, LDA_ZeroPage)
{
    ASSERT_EQ(m_cpu.pc, 0x0200);

    auto itInstruction = Instructions::findWithTypeAndMode(InstructionType::LDA, AddressingMode::ZeroPage);

    ASSERT_NE(itInstruction, Instructions::invalid());

    pushProgramByte(itInstruction->opCode);
    pushProgramByte(0x05);

    m_mem[0x05] = 0x42;

    m_cpu.execute(itInstruction->cycles, m_mem);

    ASSERT_EQ(m_cpu.reg.a, 0x42);
}

TEST_F(TestFixtureBasicInit, LDA_ZeroPageX)
{
    ASSERT_EQ(m_cpu.pc, 0x0200);

    auto itInstruction = Instructions::findWithTypeAndMode(InstructionType::LDA, AddressingMode::ZeroPageX);

    ASSERT_NE(itInstruction, Instructions::invalid());

    pushProgramByte(itInstruction->opCode);
    pushProgramByte(0x05);
    m_cpu.reg.x = 0x05;

    m_mem[0x05 + 0x05] = 0x42;

    m_cpu.execute(itInstruction->cycles, m_mem);

    ASSERT_EQ(m_cpu.reg.a, 0x42);
}

TEST_F(TestFixtureBasicInit, LDA_Absolute)
{
    ASSERT_EQ(m_cpu.pc, 0x0200);

    auto itInstruction = Instructions::findWithTypeAndMode(InstructionType::LDA, AddressingMode::Absolute);

    ASSERT_NE(itInstruction, Instructions::invalid());

    pushProgramByte(itInstruction->opCode);
    pushProgramWord(0x1080);
    m_mem[0x1080] = 0x42;

    m_cpu.execute(itInstruction->cycles, m_mem);

    ASSERT_EQ(m_cpu.reg.a, 0x42);
}

TEST_F(TestFixtureBasicInit, STA_ZeroPage)
{
    auto itInstruction = Instructions::findWithTypeAndMode(InstructionType::STA, AddressingMode::ZeroPage);

    ASSERT_NE(itInstruction, Instructions::invalid());

    pushProgramByte(itInstruction->opCode);
    pushProgramByte(0x05);
    m_cpu.reg.a = 0x42;

    m_cpu.execute(itInstruction->cycles, m_mem);

    ASSERT_EQ(m_mem[0x05], 0x42);
}

TEST_F(TestFixtureBasicInit, STA_ZeroPageX)
{
    m_cpu.reg.a = 0x42;

    auto itInstruction = Instructions::findWithTypeAndMode(InstructionType::STA, AddressingMode::ZeroPageX);

    ASSERT_NE(itInstruction, Instructions::invalid());

    pushProgramByte(itInstruction->opCode);
    pushProgramByte(0x05);
    m_cpu.reg.a = 0x42;
    m_cpu.reg.x = 0x05;

    m_cpu.execute(itInstruction->cycles, m_mem);

    ASSERT_EQ(m_mem[0x05 + 0x05], 0x42);
}

TEST_F(TestFixtureBasicInit, STA_Absolute)
{
    auto itInstruction = Instructions::findWithTypeAndMode(InstructionType::STA, AddressingMode::Absolute);

    ASSERT_NE(itInstruction, Instructions::invalid());

    pushProgramByte(itInstruction->opCode);
    pushProgramWord(0x1080);
    m_cpu.reg.a = 0x42;

    m_cpu.execute(itInstruction->cycles, m_mem);

    ASSERT_EQ(m_mem[0x1080], 0x42);
}

TEST_F(TestFixtureBasicInit, STA_AbsoluteX)
{
    m_cpu.reg.a = 0x42;

    auto itInstruction = Instructions::findWithTypeAndMode(InstructionType::STA, AddressingMode::AbsoluteX);

    ASSERT_NE(itInstruction, Instructions::invalid());

    pushProgramByte(itInstruction->opCode);
    pushProgramWord(0x1080);
    m_cpu.reg.a = 0x42;
    m_cpu.reg.x = 0x05;

    m_cpu.execute(itInstruction->cycles, m_mem);

    ASSERT_EQ(m_mem[0x1080 + 0x05], 0x42);
}

TEST_F(TestFixtureBasicInit, ADC_Immediate_Basic)
{
    auto itInstruction = Instructions::findWithTypeAndMode(InstructionType::ADC, AddressingMode::Immediate);

    ASSERT_NE(itInstruction, Instructions::invalid());

    pushProgramByte(itInstruction->opCode);
    pushProgramByte(0x05);
    m_cpu.reg.a = 0x05;

    m_cpu.execute(itInstruction->cycles, m_mem);

    ASSERT_EQ(m_cpu.reg.a, 10);
}

TEST_F(TestFixtureBasicInit, ADC_Immediate_Overflow)
{
    auto itInstruction = Instructions::findWithTypeAndMode(InstructionType::ADC, AddressingMode::Immediate);

    ASSERT_NE(itInstruction, Instructions::invalid());

    pushProgramByte(itInstruction->opCode);
    pushProgramByte(0xC8);
    m_cpu.reg.a = 0xC8;

    m_cpu.execute(itInstruction->cycles, m_mem);

    ASSERT_EQ(m_cpu.reg.a, 0x90);
    ASSERT_TRUE(m_cpu.flags.carry);
}

TEST_F(TestFixtureBasicInit, JMP_Absolute)
{
    auto itInstruction = Instructions::findWithTypeAndMode(InstructionType::JMP, AddressingMode::Absolute);

    ASSERT_NE(itInstruction, Instructions::invalid());

    pushProgramByte(itInstruction->opCode);
    pushProgramWord(0x4080);    // = 16512

    m_cpu.execute(itInstruction->cycles, m_mem);

    ASSERT_EQ(m_cpu.pc, 0x4080);
}

TEST_F(TestFixtureBasicInit, TAX_TAY)
{
    m_cpu.reg.a = 0xA1;

    ASSERT_EQ(m_cpu.reg.x, 0);
    ASSERT_EQ(m_cpu.reg.y, 0);

    int cycles = 0;
    for (auto type : { InstructionType::TAX, InstructionType::TAY })
    {
        auto itInstruction = Instructions::findWithTypeAndMode(type, AddressingMode::Implicit);
        ASSERT_NE(itInstruction, Instructions::invalid());
        pushProgramByte(itInstruction->opCode);
        cycles += itInstruction->cycles;
    }
    m_cpu.execute(cycles, m_mem);

    ASSERT_EQ(m_cpu.reg.x, 0xA1);
    ASSERT_EQ(m_cpu.reg.y, 0xA1);
}

TEST_F(TestFixtureBasicInit, TXA_TYA)
{
    m_cpu.reg.x = 0xB1;
    //m_cpu.reg.y = 0xB2;

    ASSERT_EQ(m_cpu.reg.a, 0);

    auto itInstruction = Instructions::findWithTypeAndMode(InstructionType::TXA, AddressingMode::Implicit);
    ASSERT_NE(itInstruction, Instructions::invalid());
    pushProgramByte(itInstruction->opCode);
    m_cpu.execute(itInstruction->cycles, m_mem);

    ASSERT_EQ(m_cpu.reg.a, 0xB1);
}

TEST_F(TestFixtureBasicInit, FlagsBytes)
{
    for (byte_t i = 0; i <= 127; ++i)
    {
        m_cpu.setFlagsFromByte(i);
        byte_t byte = m_cpu.flagsToByte();
        ASSERT_EQ(byte, i);
    }
}

TEST_F(TestFixtureBasicInit, AND_Immediate)
{
    auto itInstruction = Instructions::findWithTypeAndMode(InstructionType::AND, AddressingMode::Immediate);

    ASSERT_NE(itInstruction, Instructions::invalid());

    pushProgramByte(itInstruction->opCode);
    pushProgramByte(0b10101010);
    m_cpu.reg.a =   0b11001100;
                  //0b10001000
    m_cpu.execute(itInstruction->cycles, m_mem);

    EXPECT_EQ(0b10101010 & 0b11001100, 0b10001000);

    ASSERT_EQ(m_cpu.reg.a, 0b10001000);
}

TEST_F(TestFixtureBasicInit, BNE)
{
    auto itInstruction = Instructions::findWithTypeAndMode(InstructionType::BNE, AddressingMode::Relative);

    ASSERT_NE(itInstruction, Instructions::invalid());

    auto pc = m_cpu.pc;

    pushProgramByte(itInstruction->opCode);
    pushProgramByte(0);
    m_cpu.flags.zero = true;
    m_cpu.execute(itInstruction->cycles, m_mem);

    EXPECT_EQ(pc + 2, m_cpu.pc);
}

TEST_F(TestFixtureBasicInit, TheBigTest)
{
    m_cpu.pc = 0x400;

    std::string fileName("6502_functional_test.bin");
    std::ifstream fstream(fileName, std::ios::in | std::ios::binary);

    if (!fstream.is_open())
    {
        throw std::runtime_error(fmt::format("failed to open file {}", fileName));
        std::cerr << __FILE__ << ":" << __LINE__ << " file not open\n";
        return;
    }

    fstream.read(reinterpret_cast<char *>(&m_mem[0x000A]), 65526);

    m_mem.dumpToFile("6502_functional_test.dat");

    m_cpu.execute(CycleCounter::forEver, m_mem);

    std::cout << "PC: " << m_cpu.pc << "\n";
}



//TEST_F(TestFixtureBasicInit, TAX_TAY_TXA_TYA)


int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
