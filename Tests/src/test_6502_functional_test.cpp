#include "Emu6502/cpu.h"

#include <format>
#include <gtest/gtest.h>

#include <fstream>


TEST(test, _6502_functional_test)
{
    Mem mem;
    Cpu cpu;

    mem.set_word(0xFFFC, 0x200);
    cpu.reset(mem);
    cpu.pc = 0x400;

    std::string filename("Tests/src/6502_65C02_functional_tests/bin_files/6502_functional_test.bin");

    std::ifstream fstream(filename, std::ios::in | std::ios::binary);

    if (!fstream.is_open())
    {
        throw std::runtime_error(std::format("failed to open file {}", filename));
    }

    fstream.read(reinterpret_cast<char *>(&mem.data), mem.data.size());

    EXPECT_FALSE(cpu.execute(CycleCounter::FOREVER, mem));

    EXPECT_EQ(cpu.pc, 0x3469);

    mem.dump_to_file("6502_functional_test.dat");

    std::cout << "PC: " << cpu.pc << "\n";
}
