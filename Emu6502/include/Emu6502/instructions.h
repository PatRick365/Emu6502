#pragma once

#include <vector>
#include <algorithm>
#include <string_view>

#include "defines.hpp"


enum class InstructionType
{
    // Load/Store Operations
    LDA, LDX, LDY, STA, STX, STY,
    // Register Transfers
    TAX, TAY, TXA, TYA,
    // Stack Operations
    TSX, TXS, PHA, PHP, PLA, PLP,
    // Logical
    AND, EOR, ORA, BIT,
    // Arithmetic
    ADC, SBC, CMP, CPX, CPY,
    // Increments & Decrements
    INC, INX, INY, DEC, DEX, DEY,
    // Shifts
    ASL, LSR, ROL, ROR,
    // Jumps & Calls
    JMP, JSR, RTS,
    // Branches
    BCC, BCS, BEQ, BMI, BNE, BPL, BVC, BVS,
    // Status Flag Changes
    CLC, CLD, CLI, CLV, SEC, SED, SEI,
    // System Functions
    BRK, NOP, RTI
};

enum class AddressingMode
{
    Implicit,
    Accumulatior,
    Immediate,
    ZeroPage,
    ZeroPageX,
    ZeroPageY,
    Relative,
    Absolute,
    AbsoluteX,
    AbsoluteY,
    Indirect,
    IndirectX,
    IndirectY
};

struct Instruction
{
    byte_t opcode;
    InstructionType type;
    AddressingMode mode;
    uint32_t bytes;
    uint32_t cycles_min;
    uint32_t cycles_variance;
};

std::string_view to_string(InstructionType type);
std::string_view to_string(AddressingMode mode);

class Instructions
{
public:
    Instructions() = delete;

    static const std::vector<Instruction> list;

    static auto invalid()
    {
        return list.cend();
    }

    static auto find_with_opcode(byte_t opCode)
    {
        return std::find_if(list.cbegin(), list.cend(), [opCode](const Instruction &instruction)
        {
            return (instruction.opcode == opCode);
        });
    }

    static auto find_with_type_and_mode(InstructionType type, AddressingMode mode)
    {
        return std::find_if(list.cbegin(), list.cend(), [type, mode](const Instruction &instruction)
        {
            return ((instruction.type == type) && (instruction.mode == mode));
        });
    }
};

