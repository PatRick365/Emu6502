#pragma once

#include <cstddef>
#include <cstdint>
#include <cassert>
#include <array>
#include <string>

#include "defines.hpp"
#include "instructions.h"

#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_OFF


class Mem
{
public:
    Mem()
    {
        reset();
    }

    size_t get_size() const
    {
        return data.size();
    }

    void reset()
    {
        data.fill(0);
    }

    byte_t get_byte(word_t address) const
    {
        return data[address];
    }
    word_t get_word(word_t address) const
    {
        return data[address] | (data[address + 1] << 8);
    }

    void set_byte(word_t address, byte_t value)
    {
        data[address] = value;
    }
    void set_word(word_t address, word_t value)
    {
        data[address] = value;
        data[address + 1] = value >> 8;
    }

    byte_t &operator[](word_t address)
    {
        assert(address < data.size());
        return data[address];
    }
    byte_t operator[](word_t address) const
    {
        assert(address < data.size());
        return data[address];
    }

    void dump_to_file(const std::string &path = "massive_dump.dat") const;

    std::array<byte_t, 1024 * 64> data;
};

class CycleCounter
{
    enum class Mode { Forever, Counter };
    CycleCounter() = default;
public:
    static const CycleCounter FOREVER;

    explicit CycleCounter(int n)
        : m_mode(Mode::Counter),
          m_count(n)
    {}

    void dec(int n)
    {
        if (m_mode != Mode::Forever)
        {
            m_count -= n;
        }
    }

    int get() const { return m_count; }

    bool valid() const { return (m_mode == Mode::Forever) || m_count > 0; }
    bool is_consumed() const { return (m_mode != Mode::Forever) && m_count <= 0; }

    std::string to_string() const { return (m_mode == Mode::Forever) ? "Forever" : std::to_string(m_count); }

private:
    Mode m_mode = Mode::Forever;
    int m_count = 0;
};

struct Cpu
{
    static constexpr word_t RES = 0xFFFC;
    static constexpr word_t IRQ = 0xFFFE;
    static constexpr word_t NMI = 0xFFFA;
    static constexpr word_t STACK_OFF = 0x100;

    void reset(Mem &mem);

    byte_t fetch_byte(Mem &mem);
    word_t fetch_word(Mem &mem);
    byte_t read_byte(word_t address, const Mem &mem) const;
    word_t read_word(word_t address, const Mem &mem) const;

    void push_stack(byte_t byte, Mem &mem);
    byte_t pull_stack(Mem &mem);

    byte_t fetch_addressed_byte(AddressingMode mode, Mem &mem, CycleCounter &cycles);
    word_t fetch_addressed_word(AddressingMode mode, Mem &mem, CycleCounter &cycles);
    word_t fetch_resolve_operand(AddressingMode mode, Mem &mem, CycleCounter &cycles);
    word_t read_resolve_operand(AddressingMode mode, Mem &mem);

    byte_t &get_target_register(InstructionType type);
    byte_t &get_source_register(InstructionType type);
    bool &get_source_flag(InstructionType type);

    void set_ZN(byte_t working_reg);

    void set_status(byte_t value);
    byte_t get_status() const;

    bool execute(CycleCounter cycles, Mem &mem);

    void perform_ADC(AddressingMode mode, Mem &mem, CycleCounter &cycles);
    void perform_SBC(AddressingMode mode, Mem &mem, CycleCounter &cycles);

    void log_state() const;
    void log_state_light() const;

    std::string get_operand_string(AddressingMode mode, Mem &mem);

    size_t counter_instructions = 0;

    word_t pc = 0;

    struct Registers
    {
        byte_t a = 0;       // accumulator
        byte_t x = 0;       // index register x
        byte_t y = 0;       // index register y
        byte_t s = 0xFF;    // stack register
    };
    Registers reg;

    struct Flags
    {
        bool carry = false;
        bool zero = false;
        bool interrupt_disable = false;
        bool decimal = false;
        //bool break_cmd = false;
        bool overflow = false;
        bool negative = false;
    };
    Flags flags;
};
