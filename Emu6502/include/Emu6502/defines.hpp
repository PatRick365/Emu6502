#pragma once

#include <cstdint>


using byte_t = uint8_t;
using word_t = uint16_t;

using signed_byte_t = int8_t;
