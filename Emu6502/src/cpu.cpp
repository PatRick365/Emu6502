#include "cpu.h"

#include <fstream>
#include <limits>

#include <spdlog/spdlog.h>


static signed_byte_t toSigned(byte_t byte)
{
    return static_cast<signed_byte_t>(byte);
}

inline byte_t set_nth_bit_to(byte_t number, unsigned int n, bool x)
{
    return (number & ~(1 << n)) | (x << n);
}
inline bool get_nth_bit(byte_t number, unsigned int n)
{
    return (number >> n) & 1;
}

static bool crosses_page(word_t address1, word_t address2)
{
    return (address1 / 0xFF != address2 / 0xFF);
}

const CycleCounter CycleCounter::FOREVER;

void Cpu::reset(Mem &mem)
{
    pc = read_word(RES, mem);
    flags = Flags();
    reg = Registers();
}

byte_t Cpu::fetch_byte(Mem &mem)
{
    return mem[pc++];
}

word_t Cpu::fetch_word(Mem &mem)
{
    return fetch_byte(mem) | (fetch_byte(mem) << 8);
}

byte_t Cpu::read_byte(word_t address, const Mem &mem) const
{
    return mem[address];
}

word_t Cpu::read_word(word_t address, const Mem &mem) const
{
    return word_t(mem[address]) | (mem[address + 1] << 8);
}

//With the 6502, the stack is always on page one ($100-$1FF) and works top down.
void Cpu::push_stack(byte_t byte, Mem &mem)
{
    mem[STACK_OFF + reg.s] = byte;
    SPDLOG_INFO("  stack PUSH {:02x} to {:04x}", byte, STACK_OFF + reg.s);
    reg.s--;
}
byte_t Cpu::pull_stack(Mem &mem)
{
    reg.s++;
    byte_t byte = mem[STACK_OFF + reg.s];
    SPDLOG_INFO("  stack PULL {:02x} from {:04x}", byte, STACK_OFF + reg.s);
    return byte;
}

byte_t Cpu::fetch_addressed_byte(AddressingMode mode, Mem &mem, CycleCounter &cycles)
{
    if (mode == AddressingMode::Immediate)
    {
        return fetch_byte(mem);
    }
    else
    {
        return read_word(fetch_resolve_operand(mode, mem, cycles), mem);
    }
}

word_t Cpu::fetch_addressed_word(AddressingMode mode, Mem &mem, CycleCounter &cycles)
{
    if (mode == AddressingMode::Immediate)
    {
        return fetch_word(mem);
    }
    else
    {
        return read_word(fetch_resolve_operand(mode, mem, cycles), mem);
    }
}

word_t Cpu::fetch_resolve_operand(AddressingMode mode, Mem &mem, CycleCounter &cycles)
{
    switch (mode)
    {
    case AddressingMode::Accumulatior:
        break;
    case AddressingMode::ZeroPage:
    {
        return fetch_byte(mem);
    }
    case AddressingMode::ZeroPageX:
    {
        return byte_t(fetch_byte(mem) + reg.x);
    }
    case AddressingMode::ZeroPageY:
    {
        return byte_t(fetch_byte(mem) + reg.y);
    }
    case AddressingMode::Relative:
    {
        byte_t byte = fetch_byte(mem);
        return pc + toSigned(byte);
    }
    case AddressingMode::Absolute:
    {
        return fetch_word(mem);
    }
    case AddressingMode::AbsoluteX:
    {
        word_t f = fetch_word(mem);
        word_t address = f + reg.x;
        if (crosses_page(f, address))
        {
            cycles.dec(1);
        }
        return address;
    }
    case AddressingMode::AbsoluteY:
    {
        word_t f = fetch_word(mem);
        word_t address = f + reg.y;
        if (address / 0xFF > f / 0xFF)
        {
            cycles.dec(1);
        }
        return address;
    }
    case AddressingMode::Indirect:
    {
        word_t address = fetch_word(mem);
        return read_word(address, mem);
    }
    case AddressingMode::IndirectX:
    {
        word_t zeroL = (fetch_byte(mem) + reg.x) & 0xFF;
        word_t zeroH = (zeroL + 1) & 0xFF;
        return read_byte(zeroL, mem) + (read_byte(zeroH, mem) << 8);
    }
    case AddressingMode::IndirectY:
    {
        word_t zeroL = fetch_byte(mem);
        word_t zeroH = (zeroL + 1) & 0xFF;
        return read_byte(zeroL, mem) + (read_byte(zeroH, mem) << 8) + reg.y;
    }
    // should never be here
    case AddressingMode::Immediate:
    case AddressingMode::Implicit:
        break;
    }

    throw std::runtime_error(std::format("resolve_operand: not implemented: {}", to_string(mode)));
}

word_t Cpu::read_resolve_operand(AddressingMode mode, Mem &mem)
{
    CycleCounter cycles(0);
    word_t pc_before = pc;
    word_t address = fetch_resolve_operand(mode, mem, cycles);
    pc = pc_before;
    return address;
}


byte_t &Cpu::get_target_register(InstructionType type)
{
    switch (type)
    {
    case InstructionType::LDA:
    case InstructionType::TXA:
    case InstructionType::TYA:
        return reg.a;
    case InstructionType::LDX:
    case InstructionType::TAX:
    case InstructionType::INX:
    case InstructionType::DEX:
        return reg.x;
    case InstructionType::LDY:
    case InstructionType::TAY:
    case InstructionType::INY:
    case InstructionType::DEY:
        return reg.y;
    default:
        break;
    }

    throw std::runtime_error("Cpu::get_working_register -> InstructionType not handled: " + std::to_string(static_cast<int>(type)));
}

byte_t &Cpu::get_source_register(InstructionType type)
{
    switch (type)
    {
    case InstructionType::TAX:
    case InstructionType::TAY:
    case InstructionType::CMP:
    case InstructionType::STA:
        return reg.a;
    case InstructionType::TXA:
    case InstructionType::CPX:
    case InstructionType::STX:
        return reg.x;
    case InstructionType::TYA:
    case InstructionType::CPY:
    case InstructionType::STY:
        return reg.y;
    default:
        break;
    }

    throw std::runtime_error("Cpu::get_source_register -> InstructionType not handled: " + std::to_string(static_cast<int>(type)));
}

bool &Cpu::get_source_flag(InstructionType type)
{
    switch (type)
    {
    case InstructionType::BCC:
    case InstructionType::BCS:
        return flags.carry;
    case InstructionType::BEQ:
    case InstructionType::BNE:
        return flags.zero;
    case InstructionType::BMI:
    case InstructionType::BPL:
        return flags.negative;
    case InstructionType::BVC:
    case InstructionType::BVS:
        return flags.overflow;
    default:
        break;
    }
    throw std::runtime_error("Cpu::get_source_flag -> InstructionType not handled: " + std::to_string(static_cast<int>(type)));
}

void write_byte(word_t address, byte_t data, Mem &mem)
{
    mem.set_byte(address, data);
}

void Cpu::set_ZN(byte_t working_reg)
{
    flags.zero = (working_reg == 0);
    flags.negative = (working_reg & (1 << 7));
}

void Cpu::set_status(byte_t value)
{
    flags.carry = (value & (1 << 0));
    flags.zero = (value & (1 << 1));
    flags.interrupt_disable = (value & (1 << 2));
    flags.decimal = (value & (1 << 3));
    //flags.break_cmd = (value & (1 << 4));
    //flags.constant? = (value & (1 << 5));
    flags.overflow = (value & (1 << 6));
    flags.negative = (value & (1 << 7));
}

byte_t Cpu::get_status() const
{
    byte_t byte = 0;
    byte |= flags.carry << 0;
    byte |= flags.zero << 1;
    byte |= flags.interrupt_disable << 2;
    byte |= flags.decimal << 3;
    byte |= /*flags.breakCmd*/ true << 4;
    byte |= /*flags.constant?*/ true << 5;
    byte |= flags.overflow << 6;
    byte |= flags.negative << 7;
    return byte;
}

bool Cpu::execute(CycleCounter cycles, Mem &mem)
{
    //log_state();

    SPDLOG_INFO("executing.. cycles: {}. pc: {:#04x}/{:#04x}", cycles.to_string(), pc, mem.get_size());

    word_t pc_last_cycle = 0;
    const size_t max_trap_count = 3;
    size_t trap_count = 0;

    while (cycles.valid())
    {
        if (pc == pc_last_cycle)
        {
            ++trap_count;
            if (trap_count >= max_trap_count)
            {
                spdlog::error("exceeded max trap count of {}", trap_count);
                log_state();
                //exit(1);
                return false;
            }
        }
        else
        {
            trap_count = 0;
        }

        pc_last_cycle = pc;

        byte_t opcode = fetch_byte(mem);

        std::vector<Instruction>::const_iterator it = Instructions::find_with_opcode(opcode);

        if (it == Instructions::invalid())
        {
            SPDLOG_INFO("{:04x}: invalid opcode encountered: '{:#04x}'", pc_last_cycle, opcode);
            log_state();
            return false;
        }

        SPDLOG_INFO("{:04x}: {} {} ({})", pc_last_cycle, to_string(it->type), get_operand_string(it->mode, mem), to_string(it->mode));

        switch (it->type)
        {
        case InstructionType::LDA:
        case InstructionType::LDX:
        case InstructionType::LDY:
        {
            byte_t &working_reg = get_target_register(it->type);
            working_reg = fetch_addressed_byte(it->mode, mem, cycles);
            set_ZN(working_reg);
        }
            break;
        case InstructionType::STA:
        case InstructionType::STX:
        case InstructionType::STY:
        {
            byte_t source_reg = get_source_register(it->type);
            word_t address = fetch_resolve_operand(it->mode, mem, cycles);
            write_byte(address, source_reg, mem);
        }
            break;
        case InstructionType::TAX:
        case InstructionType::TAY:
        case InstructionType::TXA:
        case InstructionType::TYA:
        {
            byte_t &working_reg = get_target_register(it->type);
            working_reg = get_source_register(it->type);
            set_ZN(working_reg);
        }
            break;
        case InstructionType::ADC:
        {
            perform_ADC(it->mode, mem, cycles);
        }
            break;
        case InstructionType::SBC:
        {
            perform_SBC(it->mode, mem, cycles);
        }
            break;
        case InstructionType::CMP:
        case InstructionType::CPY:
        case InstructionType::CPX:
        // {
        //     byte_t byte = getAddressedByte(it->mode, mem);
        //     byte_t reg = getSourceRegister(it->type);
        //     flags.carry = (reg >= byte); // reset carry flag?
        //     flags.zero = (reg == byte);
        //     flags.negative = (byte & 0b10000000) > 0;
        // }
        {
            byte_t byte = fetch_addressed_byte(it->mode, mem, cycles);
            byte_t reg = get_source_register(it->type);
            signed_byte_t result = toSigned(reg) - toSigned(byte);
            flags.carry = (reg >= byte); // reset carry flag?
            flags.zero = (result == 0);
            flags.negative = result < 0;
        }
            break;
        case InstructionType::INC:
        {
            word_t addr = fetch_resolve_operand(it->mode, mem, cycles);
            byte_t byte = ++mem[addr];
            set_ZN(byte);
        }
            break;
        case InstructionType::DEC:
        {
            word_t addr = fetch_resolve_operand(it->mode, mem, cycles);
            byte_t byte = --mem[addr];
            set_ZN(byte);
        }
            break;
        case InstructionType::DEX:
        case InstructionType::DEY:
        {
            byte_t &working_reg = get_target_register(it->type);
            --working_reg;
            set_ZN(working_reg);
        }
            break;
        case InstructionType::INX:
        case InstructionType::INY:
        {
            byte_t &working_reg = get_target_register(it->type);
            ++working_reg;
            set_ZN(working_reg);
        }
            break;
        case InstructionType::JMP:
        {
            pc = fetch_resolve_operand(it->mode, mem, cycles);
        }
            break;
        // The JSR instruction pushes the address (minus one) of the return point on to the stack and then sets the program counter to the target memory address.
        case InstructionType::JSR:
        {
            word_t new_pc = fetch_resolve_operand(it->mode, mem, cycles);
            byte_t high = (pc >> 8) & 0xFF;
            byte_t low = (pc - 1) & 0xFF;
            push_stack(high, mem);
            push_stack(low, mem);
            pc = new_pc;
        }
            break;
        case InstructionType::RTS:
        {
            pc = (pull_stack(mem) | pull_stack(mem) << 8) + 1;
        }
            break;
        case InstructionType::BCC:
        case InstructionType::BNE:
        case InstructionType::BPL:
        case InstructionType::BVC:
        {
            word_t addr = fetch_resolve_operand(it->mode, mem, cycles);
            if (!get_source_flag(it->type))
            {
                pc = addr;
            }
        }
            break;
        case InstructionType::BCS:
        case InstructionType::BEQ:
        case InstructionType::BMI:
        case InstructionType::BVS:
        {
            word_t addr = fetch_resolve_operand(it->mode, mem, cycles);
            if (get_source_flag(it->type))
            {
                pc = addr;
            }
        }
            break;
        case InstructionType::TSX:
        {
            reg.x = reg.s;
            set_ZN(reg.x);
        }
            break;
        case InstructionType::TXS:
        {
            reg.s = reg.x;
        }
            break;
        case InstructionType::PHA:
        {
            push_stack(reg.a, mem);
        }
            break;
        case InstructionType::PHP:
        {
            byte_t byte = get_status();
            push_stack(byte, mem);
        }
            break;
        case InstructionType::PLA:
        {
            reg.a = pull_stack(mem);
            set_ZN(reg.a);
        }
            break;
        case InstructionType::PLP:
        {
            byte_t byte = pull_stack(mem);
            set_status(byte);
        }
            break;
        case InstructionType::AND:
        {
            byte_t byte = fetch_addressed_byte(it->mode, mem, cycles);
            reg.a &= byte;
            set_ZN(reg.a);
        }
            break;
        case InstructionType::EOR:
        {
            byte_t byte = fetch_addressed_byte(it->mode, mem, cycles);
            reg.a ^= byte;
            set_ZN(reg.a);
        }
            break;
        case InstructionType::ORA:
        {
            byte_t byte = fetch_addressed_byte(it->mode, mem, cycles);
            reg.a |= byte;
            set_ZN(reg.a);
        }
            break;
        case InstructionType::BIT:
        {
            byte_t byte = fetch_addressed_byte(it->mode, mem, cycles);
            flags.overflow = (byte & (1 << 6));
            flags.negative = (byte & (1 << 7));
            byte &= reg.a;
            flags.zero = (byte == 0);
        }
            break;
        case InstructionType::ASL:
        {
            byte_t &byte = (it->mode == AddressingMode::Immediate)
                               ? reg.a
                               : mem[fetch_resolve_operand(it->mode, mem, cycles)];
            flags.carry = (byte & (1 << 7));
            byte = byte << 1;
            set_ZN(byte);
        }
            break;
        case InstructionType::LSR:
        {
            byte_t &byte = (it->mode == AddressingMode::Immediate)
                               ? reg.a
                               : mem[fetch_resolve_operand(it->mode, mem, cycles)];
            flags.carry = (byte & (1 << 0));
            byte = byte >> 1;
            set_ZN(byte);
        }
            break;
        case InstructionType::ROL:
        {
            byte_t &byte = (it->mode == AddressingMode::Immediate)
                               ? reg.a
                               : mem[fetch_resolve_operand(it->mode, mem, cycles)];
            bool carry_before = flags.carry;
            flags.carry = get_nth_bit(byte, 7);
            byte = byte << 1;
            byte = set_nth_bit_to(byte, 0, carry_before);
            set_ZN(byte);
        }
            break;
        case InstructionType::ROR:
        {
            byte_t &byte = (it->mode == AddressingMode::Immediate)
                               ? reg.a
                               : mem[fetch_resolve_operand(it->mode, mem, cycles)];
            bool carry_before = flags.carry;
            flags.carry = get_nth_bit(byte, 0);
            byte = byte >> 1;
            byte = set_nth_bit_to(byte, 7, carry_before);
            set_ZN(byte);
        }
            break;
        case InstructionType::CLC:
            flags.carry = false;
            break;
        case InstructionType::CLD:
            flags.decimal = false;
            break;
        case InstructionType::CLI:
            flags.interrupt_disable = false;
            break;
        case InstructionType::CLV:
            flags.overflow = false;
            break;
        case InstructionType::SEC:
            flags.carry = true;
            break;
        case InstructionType::SED:
            flags.decimal = true;
            break;
        case InstructionType::SEI:
            flags.interrupt_disable = true;
            break;
        case InstructionType::NOP:
            break;
        case InstructionType::BRK:
        {
            byte_t high = (pc >> 8) & 0xFF;
            byte_t low = (pc + 1) & 0xFF;
            push_stack(high, mem);
            push_stack(low, mem);
            byte_t status = get_status();
            status |= true << 4; // break
            push_stack(status, mem);
            flags.interrupt_disable = true;
            pc = read_word(IRQ, mem);
        }
            break;
        case InstructionType::RTI:
        {
            byte_t status = pull_stack(mem);
            pc = (pull_stack(mem) | pull_stack(mem) << 8);
            set_status(status);
        }
            break;
        }

        ++counter_instructions;

        log_state_light();

        cycles.dec(it->cycles_min);
    }
    if (!cycles.is_consumed())
    {
        throw std::runtime_error(fmt::format("cycles not consumed evenly. counter: {}", cycles.get()));
    }

    return true;
}

void Cpu::perform_ADC(AddressingMode mode, Mem &mem, CycleCounter &cycles)
{
    byte_t m = fetch_addressed_byte(mode, mem, cycles);
    unsigned int tmp = m + reg.a + (flags.carry ? 1 : 0);
    flags.zero = ((tmp & 0xFF) == 0);
    if (flags.decimal)
    {
        if (((reg.a & 0xF) + (m & 0xF) + (flags.carry ? 1 : 0)) > 9) tmp += 6;
        flags.negative = tmp & 0x80;
        flags.overflow = (!((reg.a ^ m) & 0x80) && ((reg.a ^ tmp) & 0x80));
        if (tmp > 0x99)
        {
            tmp += 96;
        }
        flags.carry = (tmp > 0x99);
    }
    else
    {
        flags.negative = (tmp & 0x80);
        flags.overflow = (!((reg.a ^ m) & 0x80) && ((reg.a ^ tmp) & 0x80));
        flags.carry = (tmp > 0xFF);
    }

    reg.a = tmp & 0xFF;
}

void Cpu::perform_SBC(AddressingMode mode, Mem &mem, CycleCounter &cycles)
{
    byte_t m = fetch_addressed_byte(mode, mem, cycles);
    unsigned int tmp = reg.a - m - (flags.carry ? 0 : 1);
    //SET_NEGATIVE(tmp & 0x80);
    flags.negative = (tmp & (1 << 7));
    //SET_ZERO(!(tmp & 0xFF));
    flags.zero = ((tmp & 0xFF) == 0);
    //SET_OVERFLOW(((reg.a ^ tmp) & 0x80) && ((reg.a ^ m) & 0x80));
    flags.overflow = ((reg.a ^ tmp) & 0x80) && ((reg.a ^ m) & 0x80);

    if (flags.decimal)
    {
        if ( ((reg.a & 0x0F) - (flags.carry ? 0 : 1)) < (m & 0x0F)) tmp -= 6;
        if (tmp > 0x99)
        {
            tmp -= 0x60;
        }
    }
    flags.carry = (tmp < 0x100);
    reg.a = (tmp & 0xFF);
    return;
}

void Cpu::log_state() const
{
    SPDLOG_INFO("--CPU State--- i: {}", counter_instructions);
    SPDLOG_INFO("  pc: {:04x}, sp: {:04x}, status: {:02x}", pc, STACK_OFF + reg.s, get_status());
    SPDLOG_INFO("  A: {:02x}, X: {:02x}, Y: {:02x}", reg.a, reg.x, reg.y);
    SPDLOG_INFO("  N V - B D I Z C");
    SPDLOG_INFO("  {} {} {} {} {} {} {} {}",
                static_cast<int>(flags.negative),
                static_cast<int>(flags.overflow),
                "-",
                static_cast<int>(1),
                static_cast<int>(flags.decimal),
                static_cast<int>(flags.interrupt_disable),
                static_cast<int>(flags.zero),
                static_cast<int>(flags.carry));
    SPDLOG_INFO("--CPU State---\n");
}

void Cpu::log_state_light() const
{
    SPDLOG_INFO("    sp:{:04x}, A:{:02x}, X:{:02x}, Y:{:02x} C:{} S:{:08b}", STACK_OFF + reg.s, reg.a, reg.x, reg.y, static_cast<int>(flags.carry), get_status());
}

std::string Cpu::get_operand_string(AddressingMode mode, Mem &mem)
{
    switch (mode)
    {
    case AddressingMode::Implicit:
        return {};
    case AddressingMode::Accumulatior:
        return "A";
    case AddressingMode::Immediate:
    case AddressingMode::ZeroPage:
        return std::format("${:02x}", mem[pc]);
    case AddressingMode::Relative:
        return std::format("${:04x}", pc + 1 + toSigned(mem[pc]));
    case AddressingMode::Absolute:
        return std::format("${:04x}", read_word(pc, mem));
    case AddressingMode::ZeroPageX:
        return std::format("${:02x},x (x:{:02x} -> {:04x})", mem[pc], reg.x, read_resolve_operand(mode, mem));
    case AddressingMode::ZeroPageY:
        return std::format("${:02x},y (y:{:02x} -> {:04x})", mem[pc], reg.y, read_resolve_operand(mode, mem));
    case AddressingMode::AbsoluteX:
        return std::format("${:04x},x (x:{:02x} -> {:04x})", mem.get_word(pc), reg.x, read_resolve_operand(mode, mem));
    case AddressingMode::AbsoluteY:
        return std::format("${:04x},y (y:{:02x} -> {:04x})", mem.get_word(pc), reg.y, read_resolve_operand(mode, mem));
    case AddressingMode::IndirectX:
        return std::format("(${:02x},x) (x:{:02x} -> {:04x})", mem[pc], reg.x, read_resolve_operand(mode, mem));
    case AddressingMode::IndirectY:
        return std::format("(${:02x}),y (y:{:02x} -> {:04x})", mem[pc], reg.y, read_resolve_operand(mode, mem));
    case AddressingMode::Indirect:
        return std::format("(${:04x})", read_word(pc, mem));
    }
    return ":-/";
}

void Mem::dump_to_file(const std::string &path) const
{
    std::ofstream file(path, std::ios::binary);
    file.write(reinterpret_cast<const char *>(data.data()), data.size());
    file.close();
}

