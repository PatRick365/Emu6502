#include "instructions.h"


static Instruction make_instruction(byte_t opCode, InstructionType type, AddressingMode mode, uint32_t bytes, uint32_t cycles, uint32_t cyclesVariance = 0)
{
    return { opCode, type, mode, bytes, cycles, cyclesVariance };
}

const std::vector<Instruction> Instructions::list =
{
    /// LDA
    make_instruction(0xA9, InstructionType::LDA, AddressingMode::Immediate, 2, 2),
    make_instruction(0xA5, InstructionType::LDA, AddressingMode::ZeroPage, 2, 3),
    make_instruction(0xB5, InstructionType::LDA, AddressingMode::ZeroPageX, 2, 4),
    make_instruction(0xAD, InstructionType::LDA, AddressingMode::Absolute, 3, 4),
    make_instruction(0xBD, InstructionType::LDA, AddressingMode::AbsoluteX, 3, 4, 1),
    make_instruction(0xB9, InstructionType::LDA, AddressingMode::AbsoluteY, 3, 4, 1),
    make_instruction(0xA1, InstructionType::LDA, AddressingMode::IndirectX, 2, 6),
    make_instruction(0xB1, InstructionType::LDA, AddressingMode::IndirectY, 2, 5, 1),

    /// LDX
    make_instruction(0xA2, InstructionType::LDX, AddressingMode::Immediate, 2, 2),
    make_instruction(0xA6, InstructionType::LDX, AddressingMode::ZeroPage, 2, 3),
    make_instruction(0xB6, InstructionType::LDX, AddressingMode::ZeroPageY, 2, 4),
    make_instruction(0xAE, InstructionType::LDX, AddressingMode::Absolute, 3, 4),
    make_instruction(0xBE, InstructionType::LDX, AddressingMode::AbsoluteY, 3, 4, 1),

    /// LDY
    make_instruction(0xA0, InstructionType::LDY, AddressingMode::Immediate, 2, 2),
    make_instruction(0xA4, InstructionType::LDY, AddressingMode::ZeroPage, 2, 3),
    make_instruction(0xB4, InstructionType::LDY, AddressingMode::ZeroPageX, 2, 4),
    make_instruction(0xAC, InstructionType::LDY, AddressingMode::Absolute, 3, 4),
    make_instruction(0xBC, InstructionType::LDY, AddressingMode::AbsoluteX, 3, 4, 1),

    /// STA
    make_instruction(0x85, InstructionType::STA, AddressingMode::ZeroPage, 2, 3),
    make_instruction(0x95, InstructionType::STA, AddressingMode::ZeroPageX, 2, 4),
    make_instruction(0x8D, InstructionType::STA, AddressingMode::Absolute, 3, 4),
    make_instruction(0x9D, InstructionType::STA, AddressingMode::AbsoluteX, 3, 5),
    make_instruction(0x99, InstructionType::STA, AddressingMode::AbsoluteY, 3, 5),
    make_instruction(0x81, InstructionType::STA, AddressingMode::IndirectX, 2, 6),
    make_instruction(0x91, InstructionType::STA, AddressingMode::IndirectY, 2, 6),

    /// STX
    make_instruction(0x8E, InstructionType::STX, AddressingMode::Absolute, 3, 4),
    make_instruction(0x86, InstructionType::STX, AddressingMode::ZeroPage, 2, 3),
    make_instruction(0x96, InstructionType::STX, AddressingMode::ZeroPageY, 2, 4),

    /// STY
    make_instruction(0x8C, InstructionType::STY, AddressingMode::Absolute, 3, 4),
    make_instruction(0x84, InstructionType::STY, AddressingMode::ZeroPage, 2, 3),
    make_instruction(0x94, InstructionType::STY, AddressingMode::ZeroPageX, 2, 4),

    /// TAX, TAY, TXA, TYA
    make_instruction(0xAA, InstructionType::TAX, AddressingMode::Implicit, 1, 2),
    make_instruction(0xA8, InstructionType::TAY, AddressingMode::Implicit, 1, 2),
    make_instruction(0x8A, InstructionType::TXA, AddressingMode::Implicit, 1, 2),
    make_instruction(0x98, InstructionType::TYA, AddressingMode::Implicit, 1, 2),

    /// TSX, TXS, PHA, PHP, PLA, PLP
    make_instruction(0xBA, InstructionType::TSX, AddressingMode::Implicit, 1, 2),
    make_instruction(0x9A, InstructionType::TXS, AddressingMode::Implicit, 1, 2),
    make_instruction(0x48, InstructionType::PHA, AddressingMode::Implicit, 1, 3),
    make_instruction(0x08, InstructionType::PHP, AddressingMode::Implicit, 1, 3),
    make_instruction(0x68, InstructionType::PLA, AddressingMode::Implicit, 1, 4),
    make_instruction(0x28, InstructionType::PLP, AddressingMode::Implicit, 1, 4),

    /// AND
    make_instruction(0x29, InstructionType::AND, AddressingMode::Immediate, 2, 2),
    make_instruction(0x25, InstructionType::AND, AddressingMode::ZeroPage, 2, 3),
    make_instruction(0x35, InstructionType::AND, AddressingMode::ZeroPageX, 2, 4),
    make_instruction(0x2D, InstructionType::AND, AddressingMode::Absolute, 3, 4),
    make_instruction(0x3D, InstructionType::AND, AddressingMode::AbsoluteX, 3, 4, 1),
    make_instruction(0x39, InstructionType::AND, AddressingMode::AbsoluteY, 3, 4, 1),
    make_instruction(0x21, InstructionType::AND, AddressingMode::IndirectX, 2, 6),
    make_instruction(0x31, InstructionType::AND, AddressingMode::IndirectY, 2, 5, 1),

    /// ASL
    make_instruction(0x0A, InstructionType::ASL, AddressingMode::Immediate, 1, 2),
    make_instruction(0x0E, InstructionType::ASL, AddressingMode::Absolute, 3, 6),
    make_instruction(0x1E, InstructionType::ASL, AddressingMode::AbsoluteX, 3, 7),
    make_instruction(0x06, InstructionType::ASL, AddressingMode::ZeroPage, 2, 5),
    make_instruction(0x16, InstructionType::ASL, AddressingMode::ZeroPageX, 2, 6),

    /// ROL
    make_instruction(0x2A, InstructionType::ROL, AddressingMode::Immediate, 1, 2),
    make_instruction(0x2E, InstructionType::ROL, AddressingMode::Absolute, 3, 6),
    make_instruction(0x3E, InstructionType::ROL, AddressingMode::AbsoluteX, 3, 7),
    make_instruction(0x26, InstructionType::ROL, AddressingMode::ZeroPage, 2, 5),
    make_instruction(0x36, InstructionType::ROL, AddressingMode::ZeroPageX, 2, 6),

    /// ROR
    make_instruction(0x6A, InstructionType::ROR, AddressingMode::Immediate, 1, 2),
    make_instruction(0x6E, InstructionType::ROR, AddressingMode::Absolute, 3, 6),
    make_instruction(0x7E, InstructionType::ROR, AddressingMode::AbsoluteX, 3, 7),
    make_instruction(0x66, InstructionType::ROR, AddressingMode::ZeroPage, 2, 5),
    make_instruction(0x76, InstructionType::ROR, AddressingMode::ZeroPageX, 2, 6),

    /// ASL
    make_instruction(0x4A, InstructionType::LSR, AddressingMode::Immediate, 1, 2),
    make_instruction(0x4E, InstructionType::LSR, AddressingMode::Absolute, 3, 6),
    make_instruction(0x5E, InstructionType::LSR, AddressingMode::AbsoluteX, 3, 7),
    make_instruction(0x46, InstructionType::LSR, AddressingMode::ZeroPage, 2, 5),
    make_instruction(0x56, InstructionType::LSR, AddressingMode::ZeroPageX, 2, 6),

    /// EOR
    make_instruction(0x49, InstructionType::EOR, AddressingMode::Immediate, 2, 2),
    make_instruction(0x45, InstructionType::EOR, AddressingMode::ZeroPage, 2, 3),
    make_instruction(0x55, InstructionType::EOR, AddressingMode::ZeroPageX, 2, 4),
    make_instruction(0x4D, InstructionType::EOR, AddressingMode::Absolute, 3, 4),
    make_instruction(0x5D, InstructionType::EOR, AddressingMode::AbsoluteX, 3, 4, 1),
    make_instruction(0x59, InstructionType::EOR, AddressingMode::AbsoluteY, 3, 4, 1),
    make_instruction(0x41, InstructionType::EOR, AddressingMode::IndirectX, 2, 6),
    make_instruction(0x51, InstructionType::EOR, AddressingMode::IndirectY, 2, 5, 1),

    /// ORA
    make_instruction(0x09, InstructionType::ORA, AddressingMode::Immediate, 2, 2),
    make_instruction(0x05, InstructionType::ORA, AddressingMode::ZeroPage, 2, 3),
    make_instruction(0x15, InstructionType::ORA, AddressingMode::ZeroPageX, 2, 4),
    make_instruction(0x0D, InstructionType::ORA, AddressingMode::Absolute, 3, 4),
    make_instruction(0x1D, InstructionType::ORA, AddressingMode::AbsoluteX, 3, 4, 1),
    make_instruction(0x19, InstructionType::ORA, AddressingMode::AbsoluteY, 3, 4, 1),
    make_instruction(0x01, InstructionType::ORA, AddressingMode::IndirectX, 2, 6),
    make_instruction(0x11, InstructionType::ORA, AddressingMode::IndirectY, 2, 5, 1),

    /// BIT
    make_instruction(0x24, InstructionType::BIT, AddressingMode::ZeroPage, 2, 3),
    make_instruction(0x2C, InstructionType::BIT, AddressingMode::Absolute, 3, 4),

    ///     Arithmetic
    /// ADC
    make_instruction(0x69, InstructionType::ADC, AddressingMode::Immediate, 2, 2),
    make_instruction(0x65, InstructionType::ADC, AddressingMode::ZeroPage, 2, 3),
    make_instruction(0x75, InstructionType::ADC, AddressingMode::ZeroPageX, 2, 4),
    make_instruction(0x6D, InstructionType::ADC, AddressingMode::Absolute, 3, 4),
    make_instruction(0x7D, InstructionType::ADC, AddressingMode::AbsoluteX, 3, 4),
    make_instruction(0x79, InstructionType::ADC, AddressingMode::AbsoluteY, 3, 4),
    make_instruction(0x61, InstructionType::ADC, AddressingMode::IndirectX, 2, 6),
    make_instruction(0x71, InstructionType::ADC, AddressingMode::IndirectY, 2, 5),

    /// SBC
    make_instruction(0xE9, InstructionType::SBC, AddressingMode::Immediate, 2, 2),
    make_instruction(0xE5, InstructionType::SBC, AddressingMode::ZeroPage, 2, 3),
    make_instruction(0xF5, InstructionType::SBC, AddressingMode::ZeroPageX, 2, 4),
    make_instruction(0xED, InstructionType::SBC, AddressingMode::Absolute, 3, 4),
    make_instruction(0xFD, InstructionType::SBC, AddressingMode::AbsoluteX, 3, 4),
    make_instruction(0xF9, InstructionType::SBC, AddressingMode::AbsoluteY, 3, 4),
    make_instruction(0xE1, InstructionType::SBC, AddressingMode::IndirectX, 2, 6),
    make_instruction(0xF1, InstructionType::SBC, AddressingMode::IndirectY, 2, 5),

    /// CMP
    make_instruction(0xC9, InstructionType::CMP, AddressingMode::Immediate, 2, 2),
    make_instruction(0xC5, InstructionType::CMP, AddressingMode::ZeroPage, 2, 3),
    make_instruction(0xD5, InstructionType::CMP, AddressingMode::ZeroPageX, 2, 4),
    make_instruction(0xCD, InstructionType::CMP, AddressingMode::Absolute, 3, 4),
    make_instruction(0xDD, InstructionType::CMP, AddressingMode::AbsoluteX, 3, 4, 1),
    make_instruction(0xD9, InstructionType::CMP, AddressingMode::AbsoluteY, 3, 4, 1),
    make_instruction(0xC1, InstructionType::CMP, AddressingMode::IndirectX, 2, 6),
    make_instruction(0xD1, InstructionType::CMP, AddressingMode::IndirectY, 2, 5, 1),

    /// CPY
    make_instruction(0xC0, InstructionType::CPY, AddressingMode::Immediate, 2, 2),
    make_instruction(0xC4, InstructionType::CPY, AddressingMode::ZeroPage, 2, 3),
    make_instruction(0xCC, InstructionType::CPY, AddressingMode::Absolute, 3, 4),

    /// CPX
    make_instruction(0xE0, InstructionType::CPX, AddressingMode::Immediate, 2, 2),
    make_instruction(0xE4, InstructionType::CPX, AddressingMode::ZeroPage, 2, 3),
    make_instruction(0xEC, InstructionType::CPX, AddressingMode::Absolute, 3, 4),

    /// INC
    make_instruction(0xE6, InstructionType::INC, AddressingMode::ZeroPage, 2, 5),
    make_instruction(0xF6, InstructionType::INC, AddressingMode::ZeroPageX, 2, 6),
    make_instruction(0xEE, InstructionType::INC, AddressingMode::Absolute, 3, 6),
    make_instruction(0xFE, InstructionType::INC, AddressingMode::AbsoluteX, 3, 7),

    /// DEC
    make_instruction(0xC6, InstructionType::DEC, AddressingMode::ZeroPage, 2, 5),
    make_instruction(0xD6, InstructionType::DEC, AddressingMode::ZeroPageX, 2, 6),
    make_instruction(0xCE, InstructionType::DEC, AddressingMode::Absolute, 3, 6),
    make_instruction(0xDE, InstructionType::DEC, AddressingMode::AbsoluteX, 3, 7),

    /// Increments & Decrements
    make_instruction(0xE8, InstructionType::INX, AddressingMode::Implicit, 1, 2),
    make_instruction(0xC8, InstructionType::INY, AddressingMode::Implicit, 1, 2),
    make_instruction(0xCA, InstructionType::DEX, AddressingMode::Implicit, 1, 2),
    make_instruction(0x88, InstructionType::DEY, AddressingMode::Implicit, 1, 2),

    /// Jumps & Calls
    make_instruction(0x4C, InstructionType::JMP, AddressingMode::Absolute, 3, 3),
    make_instruction(0x6C, InstructionType::JMP, AddressingMode::Indirect, 3, 5),
    make_instruction(0x20, InstructionType::JSR, AddressingMode::Absolute, 3, 6),
    make_instruction(0x60, InstructionType::RTS, AddressingMode::Absolute, 3, 6),

    /// Branches
    make_instruction(0x90, InstructionType::BCC, AddressingMode::Relative, 2, 2, 3),
    make_instruction(0xB0, InstructionType::BCS, AddressingMode::Relative, 2, 2, 3),
    make_instruction(0xF0, InstructionType::BEQ, AddressingMode::Relative, 2, 2, 3),
    make_instruction(0x30, InstructionType::BMI, AddressingMode::Relative, 2, 2, 3),
    make_instruction(0xD0, InstructionType::BNE, AddressingMode::Relative, 2, 2, 3),
    make_instruction(0x10, InstructionType::BPL, AddressingMode::Relative, 2, 2, 3),
    make_instruction(0x50, InstructionType::BVC, AddressingMode::Relative, 2, 2, 3),
    make_instruction(0x70, InstructionType::BVS, AddressingMode::Relative, 2, 2, 3),

    /// Status Flag Changes
    make_instruction(0x18, InstructionType::CLC, AddressingMode::Implicit, 1, 2),
    make_instruction(0xD8, InstructionType::CLD, AddressingMode::Implicit, 1, 2),
    make_instruction(0x58, InstructionType::CLI, AddressingMode::Implicit, 1, 2),
    make_instruction(0xB8, InstructionType::CLV, AddressingMode::Implicit, 1, 2),
    make_instruction(0x38, InstructionType::SEC, AddressingMode::Implicit, 1, 2),
    make_instruction(0xF8, InstructionType::SED, AddressingMode::Implicit, 1, 2),
    make_instruction(0x78, InstructionType::SEI, AddressingMode::Implicit, 1, 2),

    /// System Functions
    make_instruction(0x00, InstructionType::BRK, AddressingMode::Implicit, 1, 7),
    make_instruction(0xEA, InstructionType::NOP, AddressingMode::Implicit, 1, 2),
    make_instruction(0x40, InstructionType::RTI, AddressingMode::Implicit, 1, 6)
};

std::string_view to_string(InstructionType type)
{
#define CASE_RET(x) case InstructionType::x: return #x

    switch (type)
    {
    CASE_RET(LDA);CASE_RET(LDX);CASE_RET(LDY);CASE_RET(STA);CASE_RET(STX);CASE_RET(STY);CASE_RET(TAX);CASE_RET(TAY);CASE_RET(TXA);CASE_RET(TYA);CASE_RET(TSX);CASE_RET(TXS);CASE_RET(PHA);CASE_RET(PHP);
    CASE_RET(PLA);CASE_RET(PLP);CASE_RET(AND);CASE_RET(EOR);CASE_RET(ORA);CASE_RET(BIT);CASE_RET(ADC);CASE_RET(SBC);CASE_RET(CMP);CASE_RET(CPX);CASE_RET(CPY);CASE_RET(INC);CASE_RET(INX);CASE_RET(INY);
    CASE_RET(DEC);CASE_RET(DEX);CASE_RET(DEY);CASE_RET(ASL);CASE_RET(LSR);CASE_RET(ROL);CASE_RET(ROR);CASE_RET(JMP);CASE_RET(JSR);CASE_RET(RTS);CASE_RET(BCC);CASE_RET(BCS);CASE_RET(BEQ);CASE_RET(BMI);
    CASE_RET(BNE);CASE_RET(BPL);CASE_RET(BVC);CASE_RET(BVS);CASE_RET(CLC);CASE_RET(CLD);CASE_RET(CLI);CASE_RET(CLV);CASE_RET(SEC);CASE_RET(SED);CASE_RET(SEI);CASE_RET(BRK);CASE_RET(NOP);CASE_RET(RTI);
    }
    return "invalid";

#undef CASE_RET
}

std::string_view to_string(AddressingMode mode)
{
#define CASE_RET(x) case AddressingMode::x: return #x

    switch (mode)
    {
    CASE_RET(Implicit);
    CASE_RET(Accumulatior);
    CASE_RET(Immediate);
    CASE_RET(ZeroPage);
    CASE_RET(ZeroPageX);
    CASE_RET(ZeroPageY);
    CASE_RET(Relative);
    CASE_RET(Absolute);
    CASE_RET(AbsoluteX);
    CASE_RET(AbsoluteY);
    CASE_RET(Indirect);
    CASE_RET(IndirectX);
    CASE_RET(IndirectY);
    }
    return "invalid";

#undef CASE_RET
}
